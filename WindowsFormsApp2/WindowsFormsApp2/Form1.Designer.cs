﻿namespace WindowsFormsApp2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.OppPic = new System.Windows.Forms.PictureBox();
            this.myScore = new System.Windows.Forms.TextBox();
            this.OppScore = new System.Windows.Forms.TextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.OppPic)).BeginInit();
            this.SuspendLayout();
            // 
            // OppPic
            // 
            this.OppPic.Image = global::WindowsFormsApp2.Properties.Resources.card_back_blue;
            this.OppPic.Location = new System.Drawing.Point(485, 41);
            this.OppPic.Name = "OppPic";
            this.OppPic.Size = new System.Drawing.Size(100, 111);
            this.OppPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.OppPic.TabIndex = 0;
            this.OppPic.TabStop = false;
            this.OppPic.Click += new System.EventHandler(this.pictureBox1_Click_1);
            // 
            // myScore
            // 
            this.myScore.BackColor = System.Drawing.SystemColors.Control;
            this.myScore.Location = new System.Drawing.Point(77, 13);
            this.myScore.Name = "myScore";
            this.myScore.ReadOnly = true;
            this.myScore.Size = new System.Drawing.Size(100, 20);
            this.myScore.TabIndex = 1;
            this.myScore.Text = "Your Score: 0";
            this.myScore.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // OppScore
            // 
            this.OppScore.BackColor = System.Drawing.SystemColors.Control;
            this.OppScore.Location = new System.Drawing.Point(884, 12);
            this.OppScore.Name = "OppScore";
            this.OppScore.ReadOnly = true;
            this.OppScore.Size = new System.Drawing.Size(125, 20);
            this.OppScore.TabIndex = 3;
            this.OppScore.Text = "Opponent\'s Score : 0";
            this.OppScore.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GreenYellow;
            this.ClientSize = new System.Drawing.Size(1069, 484);
            this.Controls.Add(this.OppScore);
            this.Controls.Add(this.myScore);
            this.Controls.Add(this.OppPic);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.OppPic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox OppPic;
        private System.Windows.Forms.TextBox myScore;
        private System.Windows.Forms.TextBox OppScore;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
    }
}

