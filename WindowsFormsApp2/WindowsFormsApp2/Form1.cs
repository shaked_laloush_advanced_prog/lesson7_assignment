﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;


namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
		//variable declaration
        private bool Flag = false;
        private int card = -1;
        private byte[] info;
        private string[] files;
        private int myScores = 0;
        private int OppScores = 0;
        private string cards;
        private int roundCounter = 0;
        NetworkStream clientStream;

        public Form1()
        {
			
            InitializeComponent();
			//gets the images
            string path = Directory.GetCurrentDirectory();
            path = path.Substring(0, path.LastIndexOf('\\') + 1);
            path = path.Substring(0, path.LastIndexOf('\\'));
            path = path.Substring(0, path.LastIndexOf('\\'));
            files = System.IO.Directory.GetFiles(path + @"\PNG-cards");	
			//disables controls till server inialization
			DisableControls(this);
			//creates a socket between the server and client
            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            client.Connect(serverEndPoint);
            clientStream = client.GetStream();
			//thread to get info from server
            Thread serverGet = new Thread(getInfo);
            serverGet.Start();
			//enables controls
            EnableControls(this);
			//Generates Cards
            GenerateCards();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
        private void GenerateCards()
        {
			// to creat the first place of the cards and to arrange the cards
            Point nextLocation = new Point(10, 330);

            for (int i = 0; i < 10; i++)
            {
				//current Picture Frame
                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Name = "picDynamic" + i;
                currentPic.Image = global::WindowsFormsApp2.Properties.Resources.card_back_red;
                currentPic.Location = nextLocation;
                currentPic.Size = new System.Drawing.Size(100, 114);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                //When Clicked
				currentPic.Click += delegate (object sender1, EventArgs e1)
                {
					//Checks if it's been 10 rounds(game over)
                    if (roundCounter == 10)
                    {
						//creates new game session
                        roundCounter = 0;
                        this.Hide();
                        Form1 form = new Form1();
                        form.Show();

                    }
					//gets random Card
                    Random r = new Random();
                    int rand = r.Next(1, 54);
					//Checks that random card is ok (not blank Card)
                    while (rand == 40 || rand == 41)
                    {
                        rand = r.Next(1, 54);
                    }
					//sets image and sends to server 
                    currentPic.ImageLocation = files[rand];
                    byte[] buffer = new ASCIIEncoding().GetBytes(rand.ToString());
                    clientStream.Write(buffer, 0, buffer.Length);
                    clientStream.Flush();
					//checks if other user already chose his card 
                    if (card == -1 || files[card] == OppPic.ImageLocation || files[card] == files[40] || files[card] == files[41])
                    {
                        cards = files[rand];
                        Flag = true;
                    }
                    else
                    {
                        cards = files[rand];
                        OppPic.ImageLocation = files[card];

                        whoWon();
                    }
                    cards = files[rand];
                };
			
                this.Controls.Add(currentPic);
                nextLocation.X += currentPic.Size.Width + 5;
            }
        }
		
        private void pictureBox1_Click_1(object sender, EventArgs e)
        {


        }
		//Disables Controls
        private void DisableControls(Control con)
        {
            foreach (Control c in con.Controls)
            {
                DisableControls(c);
            }
            con.Enabled = false;
        }
		//Enables Controls 
        private void EnableControls(Control con)
        {
            if (con != null)
            {
                con.Enabled = true;
                EnableControls(con.Parent);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
		//gets info from server 
        private void getInfo()
        {
            int counter = 0;
			
			//ALl of the time 
            while (1 == 1)
            {
                info = new byte[4096];
                string result;
				//gets info and turns it to string 
                int bytesRead = clientStream.Read(info, 0, 4096);
                result = System.Text.Encoding.UTF8.GetString(info);
				// checks if user already chose a card 
                if (counter > 0 && Flag == true)
                {
                    OppPic.ImageLocation = files[int.Parse(result)];
                    Flag = false;
                    whoWon();
                }
                else if (counter > 0)
                {
                    card = int.Parse(result);
                }
                counter++;
                clientStream.Flush();

            }
        }
		//checks who won and adds the points 
        private void whoWon()
        {
            string mycard = cards;

            string oppcard = OppPic.ImageLocation;
			//gets the cards values 
            oppcard = oppcard.Split('\\').Last();
            oppcard = oppcard.Split('.').First();
            mycard = mycard.Split('\\').Last();
            mycard = mycard.Split('.').First();
            mycard = mycard.Split('_').First();
            oppcard = oppcard.Split('_').First();
			//cards that have a name and not a number
            if (mycard == "ace")
            {
                mycard = "100";
            }
            if (mycard == "jack")
            {
                mycard = "11";
            }
            if (mycard == "king")
            {
                mycard = "13";
            }
            if (mycard == "queen")
            {
                mycard = "12";
            }
            if (oppcard == "ace")
            {
                oppcard = "100";
            }
            if (oppcard == "jack")
            {
                oppcard = "11";
            }
            if (oppcard == "king")
            {
                oppcard = "13";
            }
            if (oppcard == "queen")
            {
                oppcard = "12";
            }
			
			//adds points to the right user 
            int myCardi = int.Parse(mycard);
            int oppCardi = int.Parse(oppcard);
            roundCounter++;
			//sets score to the right user 
            if (myCardi > oppCardi)
            {
                myScores++;
                myScore.Text = "Your Score:" + myScores;
            }
            else if (oppCardi > myCardi)
            {
                OppScores++;         
                OppScore.Text = "Opponents Score:" + OppScores;

            }
			//checks if game over 
            if (roundCounter == 10)
            {
				//tells the right person that won that he won 
                if (myScores > OppScores)
                {
                    DialogResult result;
                    result = MessageBox.Show(this, "You Won!\n To start again press one of the cards", "");


                }
                else if (OppScores > myScores)
                {
                    DialogResult result;
                    result = MessageBox.Show(this, "You Lost!\n To start again press one of the cards", "");
                }
                else
                {
                    DialogResult result;
                    result = MessageBox.Show(this, "Tie!\n To start again press one of the cards", "");
                }

            }
        }

    }
}

